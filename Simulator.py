from __future__ import division
import networkx as nx
from numpy import round, append
from numpy.random import randint, seed
from tools_analysis import get_nodes_with_strategy


#-----------------------------------------------------------------------------
# Possible initial distributions of strategies.
#-----------------------------------------------------------------------------


def _init_random(graph, n_c_init):
    """ 
    Choose <n_c_init> random nodes in the graph <graph> to have strategy 0.
    """
    node_list = range(graph.number_of_nodes())
    n = 0
    while n < n_c_init:
        idx = randint(0, graph.number_of_nodes())
        if idx in node_list:
            graph.node[idx]['strategy'] = 0
            node_list.remove(idx)
            n += 1
    return graph
    
    
def _init_hubs(graph, n_c_init):
    """ 
    Choose the <n_c_init> nodes in the graph <graph> with highest degree to 
    have strategy 0.
    """
    a,b = nx.hits(graph)
    idxs = sorted(b, key=b.get, reverse=True)[:n_c_init]
    for i in idxs:
        graph.node[i]['strategy'] = 0
    return graph
    
    
def _init_cluster(graph, n_c_init):
    """
    Choose one random node to start a cluster of nodes with strategy 0 and set 
    neighbours of the cluster to strategy 0 until the desired number <n_c_init>  
    is reached.
    """
    i = randint(0,graph.number_of_nodes())
    graph.node[i]['strategy'] = 0
    nAdded = 1
    while nAdded < n_c_init:
        nn = graph.neighbors(i)
        j = 0
        while nAdded < n_c_init and j < len(nn):
            if graph.node[nn[j]]['strategy'] == 1:
                graph.node[nn[j]]['strategy'] = 0
                nAdded += 1
            j += 1
        i = nn[randint(0,len(nn))]
    return graph
    
    
def _init_spokes(graph, n_c_init):
    """ 
    Choose the <n_c_init> nodes in the graph <graph> with smallest degree to 
    have strategy 0.
    """
    a,b = nx.hits(graph)
    idxs = sorted(b, key=b.get, reverse=False)[:n_c_init]
    for i in idxs:
        graph.node[i]['strategy'] = 0
    return graph
    

inits = {'init_random': _init_random,
         'init_hubs': _init_hubs,
         'init_cluster': _init_cluster,
         'init_spokes' : _init_spokes}


#-----------------------------------------------------------------------------
# The Simulator class.
#-----------------------------------------------------------------------------
        
        
class Simulator():
    
    def __init__(self, init, f_c, graph, payoff, seed_id=None):
        """
        
        Initialize the simulator.
        
        Parameters
        ----------
        init : str
            String indicating which initializer for the initial strategy 
            configuration is to be used.
        f_c : float between 0 and 1
            Fraction of nodes having strategy 0 in the initial configuration.
        graph : networkx.classes.graph.Graph
            The graph representing the interactions between the nodes.
        payoff : numpy.ndarray of floats
            Payoff Matrix characterizing the reward each individual gets from 
            interactions depending of the involved strategies.
        seed_id : int
            Seed for the random generator.
        """
        
        # --- If a seed number for the random number generator is given, seet seed. ---
        if seed_id:
            seed(seed_id)
        
        # --- Determine number of nodes with strategy 0 in the initial configuration. ---
        n_c_init = int(round(graph.number_of_nodes()*f_c))
        
        # --- Initialize graph. ---
        self.graph = graph.copy()        
        nx.set_node_attributes(self.graph, 'strategy', 1)
        nx.set_node_attributes(self.graph, 'fitness', 1)
        self.graph = inits[init](self.graph, n_c_init)
         
        # --- Initialize internal data. ---
        self.payoff = payoff
        self.n_c = [n_c_init] # List of number of nodes with strategy 0 (one entry
                              # per iteration if run() is called with <track> set
                              # to True, else only initial and final number).
        self.n_i = 0          # Number of iterations performed.
        self.alterable = []   # List of all nodes, that have at least one 
                              # neighbor with a different strategy than its own
                              # (=^ all nodes that can possibly change in the 
                              # next iteration).
        for n in self.graph.node:
            neighbors = self.graph.neighbors(n)
            for nn in neighbors:
                if self.graph.node[n]['strategy'] != self.graph.node[nn]['strategy']:
                    self.alterable += [n]
                    break
        
        # --- Initialize fitness ---
        self._play()
        

    def _play(self):
        """
        Let each node interact with all its neighbours and update fitness 
        accordingly.
        """
        edges = self.graph.edges()
        for i in range(len(edges)):
            n1 = self.graph.node[edges[i][0]]
            n2 = self.graph.node[edges[i][1]]
            n1['fitness'] += self.payoff[n1['strategy']][n2['strategy']]
            n2['fitness'] += self.payoff[n2['strategy']][n1['strategy']]
            
        
    def run(self,n_iter=50000, optimized=True, track=False):
        """
        Execute <n_iter> iterations of the game. In each iteration, one 
        individual is chosen randomly and it's strategy is changed to the
        strategy of its neighbour with the highest fitness.
        
        Parameters
        ----------
        n_iter : int
            Maximal number of iterations.
        optimized : bool
            If True, only nodes that have at least one neighbour with another
            strategy will be chosen for update, thus saving computation time 
            but artificially increasing the speed of evolution.
            (Setting track to True makes only sense when optimized is False.)
        track : bool
            If True, the number of nodes with strategy 0 in the game will be 
            recorded at each iteration.
        """
        #seed(12345)
        
        unaltered_alterables = set() # In the set <unaltered_alterables>, sequent
                                     # nodes that did not change strategy will be
                                     # saved (only if they are among the alterable 
                                     # nodes). As soon as a node that changed is 
                                     # found, this set will be set to the empty 
                                     # set again. Thus we can keep track of how 
                                     # many nodes did not change in sequence and 
                                     # stop if this number is equal to the number 
                                     # of nodes that could possibly change 
                                     # (because of having neighbours with different
                                     # strategies).
        
        for i in range(n_iter):
            
            # --- If alterable nodes exist, update one random node, else exit. ---
            if not self.alterable:
                break
            else:
                    
                # --- Choose a random node. ---
                if optimized:
                    # for speed: choose node only from <alterables>
                    node = self.alterable[randint(0, len(self.alterable))]
                else:
                    node = randint(0, self.graph.number_of_nodes())
                    
                # --- Update node and keep track of unaltered alterables. ---
                if not self._update_node(node,track):
                    if node in self.alterable:
                        unaltered_alterables.add(node)
                else:
                    unaltered_alterables = set()
       
                # --- Exit, if all alterable nodes have been visited once 
                #     without any change of strategy. ---
                if len(unaltered_alterables) == len(self.alterable):
                    #self._check_convergence()
                    break
            
        # --- Document the changes. ---
        if not track:
            self.n_c = append(self.n_c,len(get_nodes_with_strategy(self.graph, 0)))
        self.n_i = i
        
        
    def _update_node(self,node,track=False):
        """
        Determine the new strategy of the node <node> and if it changed, update
        the fitnesses of the nodes affected by this change (the node itself and 
        its nearest neighbours).
            
        Returns
        -------
        bool
            True, when node changed its strategy; False when it did not change.
        """
            
        # --- Get successor. ---
        s = self._get_successor(node)
                        
        # --- Track number of nodes with strategy . ---
        if track:
            self._track(self.graph.node[node]['strategy'], self.graph.node[s]['strategy'])
        
        # --- If the node changed strategy, update graph. ---
        if self.graph.node[node]['strategy'] != self.graph.node[s]['strategy']:
            
            # --- Update the strategy of the node. ---
            self.graph.node[node]['strategy'] = self.graph.node[s]['strategy']
            
            # --- Update the fitness of the node and its neighbors. ---
            self._play_patch(node)
                
            # --- Update the list of alterable nodes. ---
            self._update_alterable(node)
            
            return True
                
        return False
            
            
    def _get_successor(self,node):
        """ 
        Determine the node with the highest fitness among the among the 
        neighbours of the node <node>.
        """
        
        #seed(12345)
        
        # --- Determine the nodes with highest fitness among the neighbors of the node <node>. ---
        nn = self.graph.neighbors(node)
        fi = [self.graph.node[n]['fitness'] for n in nn]
        #fi.sort()
        #print node, fi
        maxfi = max(fi)
        maxpos = [i for i,j in enumerate(fi) if j == maxfi]
        
        # --- If only one node assumes the maximum return this node. ---
        if len(maxpos) == 1:
        #    print 'single maximum - ', nn[maxpos[0]]
            return nn[maxpos[0]]
            
        # --- If there are several nodes that assume the fitness maximum, check if 
        #       there are any that have the same strategy as the original node. ---
        samestrat = []
        for i in maxpos:
            if self.graph.node[nn[i]]['strategy'] == self.graph.node[node]['strategy']:
                samestrat += [nn[i]]
                
        # --- If there are nodes with the same strategy, return any of those. ---
        if samestrat:
            if len(samestrat) == 1:
        #        print 'single samestrat maximum - ', samestrat[0]
                return samestrat[0]
            else:
                #print 'random samestrat maximum - ', samestrat[randint(0, len(samestrat))]
                return samestrat[randint(0, len(samestrat))]
                
        # --- If there are no nodes with the same strategy, choose one random. ---
        #print 'random maximum - ', nn[maxpos[randint(0, len(maxpos))]]
        return nn[maxpos[randint(0, len(maxpos))]]
        
        
    def _track(self,old_strat,new_strat):
        """
        Track the number of nodes with strategy 0 in the game each iteration by 
        comparing the old strategy <old_strat> and the new strategy <new_strat>
        of the node that changed.
        """
        if old_strat == new_strat:
            self.n_c = append(self.n_c,self.n_c[-1])
        # 
        else:
            if new_strat == 0:
                self.n_c = append(self.n_c,self.n_c[-1] + 1)
            else:
                self.n_c = append(self.n_c,self.n_c[-1] - 1)
                
        
    def _play_patch(self,node):
        """
        Reset the fitness of the node <node> and its nearest neighbours and 
        increase it according to the reward they get from the interactions with 
        their nearest neighbors.
        """
        nodelist = self.graph.neighbors(node) + [node]
        for n in nodelist:
            self.graph.node[n]['fitness'] = 1
            neighbors = self.graph.neighbors(n)
            for nn in neighbors:
                s1 = self.graph.node[n]['strategy']
                s2 = self.graph.node[nn]['strategy']
                self.graph.node[n]['fitness'] += self.payoff[s1][s2]
        

    def _update_alterable(self,node):
        """
        Keep track of which nodes are still alterable or became alterable after
        the node <node> changed its strategy.
        """
        
        nodelist = self.graph.neighbors(node) + [node] # The change of strategy
                                                       # of the node <node>
                                                       # can affect the 
                                                       # alterability of itself
                                                       # and of its neighbours.
        for n in nodelist:
            
            # --- Check if node <n> is alterable. ---
            alterable = False
            for nn in self.graph.neighbors(n):
                if self.graph.node[n]['strategy'] != self.graph.node[nn]['strategy']:
                    alterable = True
                    break
                
            # --- If the node <n> was alterable before but is not alterable
            #     anymore, remove it from <self.alterable>. ---
            if n in self.alterable:
                if not alterable:
                    self.alterable.remove(n)
                    
            # --- If the node <n> was not alterable before but is now alterable,
            #     add it to <self.alterable>. ---
            else:
                if alterable:
                    self.alterable += [n]


    def _check_alterable(self):
        """
                
        *** For debugging. ***
        
        Check if <self.alterable> has been updated correctly, i.e. if it 
        contains exactly those nodes with at least one neighbour with a 
        different strategy.
    
        Returns
        -------
        bool
            True, if <self.alterable> was found to be consistent, False otherwise.
        
        """
        
        for n in self.graph.node:
            
            # --- Check if node <n> is alterable. ---
            alterable = False
            for nn in self.graph.neighbors(n):
                if self.graph.node[n]['strategy'] != self.graph.node[nn]['strategy']:
                    alterable = True
                    break
            
            # --- If the node <n> is alterable but not in <self.alterable>,
            #     print information and return False. ---
            if alterable and not n in self.alterable:
                print 'Node ' + str(n) + ' alterable but not in list! '
                return False
            
            # --- If the node <n> is not alterable but in <self.alterable>,
            #     print information and return False. ---
            if not alterable and n in self.alterable:
                print 'Node ' + str(n) + ' not alterable but in list! '
                return False
                
        # --- All nodes were found to be consistent. ---
        return True
                
                
    def _check_convergence(self):
        """
 
        *** For debugging. ***  
        
        Check if there are any nodes that would change strategy if they were
        selected in the next iteration. This method can be used if the
        execution of the method <run()> terminated early, to make sure that
        really a fixed state has been reached. If there are no bugs, however,
        this should be unnecessary.
            
        Returns
        -------
        list
            List of nodes that would change strategy. Can also be interpreted
            as boolean: Empty lists are implicit False, non-empty lists are
            implicit True.
        """
        
        # --- Set the fitnesses to default value. ---
        for n in self.graph.node:
            self.graph.node[n]['fitness'] = 1
            
        # --- Update fitnesses by letting all nodes play against all of their 
        #     neighbours. ---
        self._play()
        
        # --- Find all nodes that would change strategy if they were selected. ---
        changing_nodes = []
        for n in self.graph.node:
            s = self._get_successor(n)
            if self.graph.node[s]['strategy'] != self.graph.node[n]['strategy']:
                changing_nodes += [n]
         
        if changing_nodes:
            print '{} changing nodes found: '.format(len(changing_nodes)) + \
                  str(changing_nodes)
        return changing_nodes