"""
Create a histogram showing the size distributions of minority cluster for two
exemplary scenarios.
"""

from __future__ import division
from matplotlib import pyplot
from numpy import arange
from networkx import random_regular_graph
from Simulator import Simulator
from tools_plotting import color_mapping
from tools_analysis import get_cluster_sizes, scenarios


def plot_clustersizes_histogram(sizes, degree, strategy):
    """
    Plot a histogram of the cluster sizes <sizes> of nodes with strategy <strategy>.
    """
    
    pyplot.figure()
    bins = arange(min(sizes)-1.5, max(sizes)+2.5)
    pyplot.hist(sizes, color=color_mapping(strategy), bins=bins)
    if strategy == 0:
        pyplot.title('Sizes of stable cooperator communities in defector majority', fontsize=40)
    else:
        pyplot.title('Sizes of stable defector communities in cooperator majority', fontsize=40)
    pyplot.xlabel('clustersizes, k = {}'.format(degree), size=40)
    pyplot.xticks(range(min(sizes)-1, max(sizes)+2), fontsize=40)
    pyplot.xlim(min(sizes)-1, max(sizes)+2)
    pyplot.yticks(fontsize=40)
    pyplot.locator_params(axis='y', nbins=1)


pyplot.close('all')


# --- Set parameters of the topology. ---
number_of_nodes = 400
degree = 6


for scenario in scenarios():
        
    # --- To collect more data and catch rare cases, run simulations on 100
    #     graphs and store all cluster sizes that occured. ---
    cluster_sizes = []
    for i in range(100):
        graph = random_regular_graph(degree, number_of_nodes)
        simulator = Simulator('init_random', 0.5, graph, scenario['payoff'])
        simulator.run(optimized=True, track=False)
        cluster_sizes += get_cluster_sizes(simulator.graph, scenario['strategy'], return_type=list)
        
    # --- If any clusters of the minority were found, plot a histogram of
    #     their sizes. ---
    if cluster_sizes:
        plot_clustersizes_histogram(cluster_sizes, degree, scenario['strategy'])