"""
Create a histogram of all occurring minority sizes in a setting with a
scale-free topology.
"""

from __future__ import division
from matplotlib import pyplot
from numpy import array, ones_like
from networkx import barabasi_albert_graph
from Simulator import Simulator
from tools_analysis import get_cluster_sizes
from tools_plotting import color_mapping


pyplot.close('all')
    
                    
def plot_minorities(cooperator_minority_sizes, defector_minority_sizes, n_nodes):
    """
    Plot a histogram of the minority sizes occurring in the final state of a
    considerable amount of simulations. The size distributions of both 
    cooperator minorities and defector minorities are normalized to the number
    of final states with the respective strategy being the minority.
    
    Parameters
    ----------
    cooperator_minority_sizes : list of lists
        The list contains a list for each simulation with a cooperator minority
        in the final state containing the corresponding cluster sizes.
    defector_minority_sizes : list of lists
        The list contains a list for each simulation with a defector minority
        in the final state containing the corresponding cluster sizes.
    n_nodes : int
        Total number of nodes in the simulation.
    """
    
    pyplot.figure()
    
    n_c_min = len(cooperator_minority_sizes)
    n_d_min = len(defector_minority_sizes)
    min_sizes = [[x for sublist in cooperator_minority_sizes for x in sublist], \
                 [x for sublist in defector_minority_sizes for x in sublist]]
    weights_c = ones_like(min_sizes[0])/n_c_min
    weights_d = ones_like(min_sizes[1])/n_d_min
    colors = [color_mapping(0),color_mapping(1)]
    bins = range(0,int(n_nodes/2)+1)
    pyplot.hist(min_sizes, weights=[weights_c,weights_d], 
                   color=colors, histtype='stepfilled', bins=bins, 
                   alpha=0.7, label=['C', 'D'], edgecolor = "none",
                   align='left', rwidth=1)

    pyplot.xlim([0,n_nodes/2])
    pyplot.xlabel('size')
    pyplot.legend(loc='upper right')
    

# --- Set parameters of the simulation. ---
n_nodes = 400
degree  = 3
payoff  = array([[ 3, 1],
                 [ 4, 2]])


# --- Number of rounds ---
n_rounds = 10000


# --- Data container for the results ---
cooperator_minority_sizes = []
defector_minority_sizes   = []


for j in range(n_rounds):
    
    # --- Simulation. ---
    graph = barabasi_albert_graph(n_nodes, degree)
    simulator = Simulator('init_random', 0.5, graph, payoff)
    simulator.run()
    
    # --- Save results. ---
    if simulator.n_c[-1] != n_nodes and simulator.n_c[-1] > n_nodes/2:
        sizes = get_cluster_sizes(simulator.graph, 1, return_type='list')
        defector_minority_sizes += [sizes]
    elif simulator.n_c[-1] != 0 and simulator.n_c[-1] < n_nodes/2:
        sizes = get_cluster_sizes(simulator.graph, 0, return_type='list')
        cooperator_minority_sizes += [sizes]


# --- Show the distribution of minorities for each initial configuration. ---
if cooperator_minority_sizes or defector_minority_sizes:
    plot_minorities(cooperator_minority_sizes, defector_minority_sizes, n_nodes)