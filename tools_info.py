from tools_analysis import get_cluster_sizes, is_prisoners_paradox


def print_simulation_parameters(topology, number_of_nodes, degree, f_c, payoff):
    """
    Print information about the simulation parameters to the console.
    """

    print '\n\n ~~~ SIMULATION PARAMETERS ~~~'
    
    print 'Topology: ' + topology + ', {} nodes with degree {}.' \
          .format(number_of_nodes,degree)
    print 'Initial cooperator distribution: {} cooperators, randomly chosen.' \
          .format(int(f_c*number_of_nodes))
    print 'Payoff:   [[ {} , {} ],'.format(payoff[0,0], payoff[0,1])
    print '           [ {} , {} ]]'.format(payoff[1,0], payoff[1,1])
    if is_prisoners_paradox(payoff):
        print 'Payoff matrix models the prisoner\'s dilemma.'
    else:
        print 'Payoff matrix does not model the prisoner\'s dilemma.'
    

def print_final_state(simulator):
    """
    Print information about the final state to the console.
    """
    
    print '\n ~~~ FINAL STATE ~~~'
    
    if not simulator._check_convergence():
        print 'Evolution reached fixed state after {} iterations.' \
              .format(simulator.n_i)
    else:
        print 'No fixed state is reached.'
    
    print 'Total number of cooperators in the final state is {}.' \
          .format(simulator.n_c[-1])
          
    sizes = get_cluster_sizes(simulator.graph,0)
    if sizes:
        print 'Cluster sizes - cooperators:'
        for s in sizes:
            print ' -- {} cluster(s) with size {}' \
                  .format(sizes[s], s)
          
    sizes = get_cluster_sizes(simulator.graph,1)
    if sizes:
        print 'Cluster sizes - defectors:'
        for s in sizes:
            print ' -- {} cluster(s) with size {}' \
                  .format(sizes[s], s)
    print '\n'