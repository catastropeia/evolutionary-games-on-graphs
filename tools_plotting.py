from __future__ import division
import networkx as nx
from matplotlib import pyplot
from tools_analysis import get_nodes_in_cluster


def color_mapping(strategy):
    mapping = {0 : 'b', 1 : 'r'}
    return mapping[strategy]
    
    
def draw_graph(graph):
    """ 
    Draw the graph <graph> with node sizes proportional to fitness and color
    the nodes according to their strategy.    
    """

    nodelist = graph.nodes()
    
    # --- Cosmetics. ---
    sizes  = [(graph.node[n]['fitness']-1) for n in nodelist]
    sizes  = [(x-0.97*min(sizes))/(max(sizes)-min(sizes)) for x in sizes]
    sizes  = [x*1200 for x in sizes]
    colors = [color_mapping(graph.node[n]['strategy']) for n in nodelist]
    layout = nx.spring_layout(graph)
    
    # --- Draw. ---
    nx.draw_networkx_edges(graph, layout, alpha=0.2)
    nx.draw_networkx_nodes(graph, layout, nodelist=nodelist, node_size=sizes, 
                           node_color=colors, alpha=0.7)
    
    
def plot_cluster_and_nn(graph, node):
    """
    Plot all nodes in the graph <graph> belonging to the same cluster as the 
    node <node>, their neighbours and the neighbours of one of the neighbours
    (for exemplification).
    """
    
    # --- Get all nodes in cluster and add their nearest neighbours. ---
    nodelist = get_nodes_in_cluster(graph, node)
    neighbours = []
    for n in nodelist:
        neighbours += graph.neighbors(n)
        
    # --- Add the neighbors of one of the neighbors. ---
    nnn = next((v for i, v in enumerate(neighbours) if v not in nodelist), -1)
    nodelist = list(set(nodelist + neighbours + graph.neighbors(nnn)))

    # --- Draw. ---    
    pyplot.figure()
    draw_graph(graph.subgraph(nodelist))
    pyplot.axis('off')
    pyplot.show()
    
    
def plot_evolution(n_c, n_nodes):
    """
    Plot the number of cooperators over time, specified by the list <n_c>. 
    Total number of nodes in the simulation is <n_nodes>.
    """
    fig = pyplot.figure()
    ax = fig.add_subplot(1, 1, 1)
    pyplot.plot(range(len(n_c)), n_c/n_nodes, 'b', linewidth=5.0)
    
    # --- Cosmetics. ---
    ax.set_xlim([0,len(n_c)])
    ax.set_ylim([0,1])
    ax.xaxis.set_ticks([0,len(n_c)])
    ax.yaxis.set_ticks([0,0.5,1.])
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    pyplot.xlabel('iterations', size=40)
    pyplot.ylabel('cooperators [%]', size=40)
    pyplot.yticks(fontsize=40)
    pyplot.xticks(fontsize=40)