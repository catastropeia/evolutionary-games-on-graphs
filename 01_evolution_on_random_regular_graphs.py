"""
Show the evolution over time of two exemplary scenarios.
"""

from __future__ import division
from matplotlib import pyplot
from networkx import random_regular_graph
from Simulator import Simulator
from tools_plotting import plot_evolution
from tools_analysis import scenarios
from tools_info import print_simulation_parameters, print_final_state

pyplot.close('all')


def cooperators_win(payoff, degree):
    """
    Determine whether the criterion derived by Tarnita et al in "Strategy 
    selection in structured populations", Journal of Theoretical Biology, 2009,
    that predicts the winning of cooperators is fulfilled.

    Parameters
    ----------
    payoff : numpy.array
        Payoff Matrix characterizing the reward each individual gets from 
        interactions depending of the involved strategies.
    degree : int
        Node degree of the graph that represents the topology.
  
    
    Returns
    -------
    bool
        True, when cooperators are predicted to win; False otherwise`.
    
    """
    sigma = (degree + 1)/(degree - 1)
    return sigma*payoff[0][0] + payoff[0][1] > payoff[1][0] + sigma*payoff[1][1]


# --- Set parameters of the topology. ---
number_of_nodes = 400
degree = 6 # number of neighbours of each node
f_c = 0.5 # fraction of cooperators in the initial state


for scenario in scenarios():

    payoff = scenario['payoff']    
    
    # --- Display information about the system. ---
    print_simulation_parameters('Random regular graph', number_of_nodes, 
                                degree, f_c, payoff)
    if cooperators_win(payoff,degree):
        print 'Prediction: Cooperators win.'
    else:
        print 'Prediction: Cooperators loose.'
    
    
    # --- Run the simulation. ---
    graph = random_regular_graph(degree, number_of_nodes, seed=1)
    simulator = Simulator('init_random', f_c, graph, payoff, seed_id=1)
    simulator.run(optimized=False, track=True)
    
    
    # --- Display result and plot the number of cooperators over time. ---
    print_final_state(simulator)
    plot_evolution(simulator.n_c, number_of_nodes)