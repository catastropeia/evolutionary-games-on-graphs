"""
Search for specific cluster sizes.
"""

from __future__ import division
from numpy import array
from matplotlib import pyplot
from networkx import random_regular_graph
from Simulator import Simulator
from tools_plotting import plot_cluster_and_nn
from tools_analysis import find_node_with_clustersize

pyplot.close('all')


def find_cluster_with_sizes(strategy, sizes, init, f_c, graph, payoff,
                            max_iter=100):
    """
    Try to find minority clusters with specific sizes.
    
    Parameters
    ----------
    strategy : int
        Strategy of the minority of which a specific cluster is wanted. 
    sizes : list of ints
        All cluster sizes which are searched for.
    init : str
        String indicating which initializer for the initial strategy 
        configuration is to be used.
    f_c : float between 0 and 1
        Fraction of nodes having strategy 0 in the initial configuration.
    graph : networkx.classes.graph.Graph
        The graph representing the interactions between the nodes.
    payoff : numpy.ndarray of floats
        Payoff matrix.
    max_iter : int
        Maximal number of simulations.

    Returns
    -------
    dict
        Dictionary with the sought sizes as keys and a Simulator object (of 
        which the final state contains a cluster with the demanded size) and 
        the index of one node belonging to the cluster as values.
    """

    results = dict()
    
    for i in range(max_iter):
        
        simulator = Simulator(init, f_c, graph, payoff)
        simulator.run()
        
        for size in sizes:
            
            # --- Check if there is any node belonging to a cluster with size
            #     <size> and if yes, save the simulator and the node in 
            #     <results> and eliminate <size> from <sizes>. ---
            candidate = find_node_with_clustersize(simulator.graph,strategy,size)
            if candidate is not -1:    
                print '\tFound cluster of size {}.'.format(size)
                results[size] = [simulator, candidate]
                sizes.remove(size)
                
        # --- Terminate if no sizes are left, i.e. minority cluster of all 
        #     given sizes were found. ---
        if not sizes:
            break
        
    if sizes:
        print '\tNo clusters with sizes {} found.'.format(sizes)
          
    return results
            

# --- Set parameters of the simulation. ---
n_nodes = 400
degree  = 6
graph   = random_regular_graph(degree, n_nodes)
f_c     = 0.5
payoff  = array([[ 3.9, 1.9],
                 [ 4,   2]])


# --- List all cluster sizes to be searched for. ---
sizes = [3]
strategy = 1


# --- Search for all sizes and plot one example each. ---
result = find_cluster_with_sizes(strategy, sizes, 'init_random', f_c, graph, payoff)
for s in result:
    plot_cluster_and_nn(result[s][0].graph,result[s][1])