from __future__ import division
from networkx import number_connected_components
from numpy import array
    
    
def scenarios():
    """
    Create two scenarios, one which favors defectors and one which favors
    cooperators.
    """
    
    scenario_defector_minorities   = {'payoff' : array([[ 3.9, 1.9],
                                                        [ 4,   2  ]]),
                                     'strategy' : 1}
    
    scenario_cooperator_minorities = {'payoff' : array([[ 3, 1],
                                                        [ 4, 2]]),
                                     'strategy' : 0}
                                     
    return [scenario_cooperator_minorities, scenario_defector_minorities]

    
def is_prisoners_paradox(payoff):
    """ 
    Determine whether the payoff matrix <payoff> models the prisoner's dilemma.
    """
    
    if payoff[1,0] > payoff[0,0] and \
       payoff[0,0] > payoff[1,1] and \
       payoff[1,1] > payoff[0,1]:
        return True
    return False
    
    
def get_nodes_with_strategy(graph,strategy):
    """ 
    Return all nodes in the graph <graph> that have the strategy <strategy>. 
    """
    
    return [n for n,ad in graph.node.items() if ad['strategy'] == strategy]
    

def get_nodes_in_cluster(graph, node):
    """
    Returns all nodes in the graph <graph> that belong to the same cluster as 
    the node <node>.
    """
    
    nodelist = [node]
    to_check = [n for n in graph.neighbors(node)
                if graph.node[n]['strategy'] == graph.node[node]['strategy']]
                    
    while to_check:
        nn = to_check[0]
        nodelist += [nn]
        to_check = to_check[1:]
        to_check += [n for n in graph.neighbors(nn)
                     if not n in to_check and not n in nodelist and \
                     graph.node[n]['strategy'] == graph.node[node]['strategy']]
        
    return nodelist
    
    
def get_cluster_sizes(graph, strategy, return_type=dict):
    """
    Determine the cluster sizes of the nodes with strategy <strategy> in the
    graph <graph>.

    Returns
    -------
    dict/list
        Dictionary or list of cluster sizes (type is determined by 
        the parameter <return_type>), where the key indicates the cluster size 
        and the value the number of occurrences of that size.
    """
    
    sizes = []
    nodelist = get_nodes_with_strategy(graph,strategy)
    
    while nodelist:
        
        # --- Get cluster to which the first node in <nodelist> belongs. ---
        cluster = get_nodes_in_cluster(graph, nodelist[0])
        sizes += [len(cluster)]
        
        # --- Remove all nodes of the cluster from <nodelist>. ---
        nodelist = [n for n in nodelist if n not in cluster]
        
    if return_type is dict:
        
        # --- Convert list of all sizes to dictionary. ---
        size_dict = dict.fromkeys(set(sizes))
        for s in set(sizes):
            size_dict[s] = len([i for i in sizes if i == s])
        sizes = size_dict
    
    return sizes
    

def find_node_with_clustersize(graph, strategy, size):
    """
    Find a node with strategy <strategy> in the graph <graph> belonging to a
    cluster of size <size>.
    
    Returns
    -------
    int
        Number of the node. -1, if no cluster of this size exists.
    """
    
    nodelist = get_nodes_with_strategy(graph,strategy)
    
    while nodelist:
        
        # --- Get cluster to which the first node in <nodelist> belongs. ---
        cluster = get_nodes_in_cluster(graph, nodelist[0])
            
        # --- If the cluster has the desired length, return its first node. ---
        if len(cluster) == size:
            return cluster[0]
        
        # --- Remove all nodes of the cluster from <nodelist>. ---
        nodelist = [n for n in nodelist if n not in cluster]
            
    return -1
    
    
def number_of_cluster(graph,strategy):
    """
    Returns the number of cluster of nodes with strategy <strategy> in the 
    graph <graph>.
    """
    
    nodelist = get_nodes_with_strategy(graph,strategy)
    subgraph = graph.subgraph(nodelist)
    return number_connected_components(subgraph)