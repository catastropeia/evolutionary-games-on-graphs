# Simulation of Evolutionary Games on Graphs

## Introduction

Evolutionary game theory is the study of survival or disappearance of different strategies that agents of some population choose when interacting with other individuals. Any agent interacting with another will get a reward which depends on its own strategy and on the strategy of its partner. The fitness of an individual is assumed to be proportional to the sum of rewards it gets from all its interactions. Some strategies may result in a higher fitnesses than others, and as fitness translates into reproductive success, the more successful strategies will become dominant.

In modelling evolutionary games it is often assumed that the agents live in a well-mixed domain, i.e. that each agent is equally likely to interact with any of the other individuals. However, this is rarely the case in reality, as the agents most likely live in some spatial arrangement, where they are more likely to interact with their immediate neighbours than with some individual far away. And if we consider higher evolved species with some kind of social life, it is also expected that individuals which are related or close will interact more often with each other.

A useful approach in modelling evolutionary games that takes possible spatial settings into account is based on graph theory. Each individual in the population is represented as a node (also called vertex) of the graph. An edge between two nodes then indicates that these two nodes interact with each other. Thus, arbitrary topologies can be created.

The code at hand allows to simulate the evolution of two competing strategies on any graph provided. What follows is first an account of the model and of the assumptions that entered the design in section 2. Next is an instruction on how to use the code in section 3 and last a discussion of exemplary runs in section 4.

## The Model

### Topology

The population is modelled as a graph, where nodes represent individuals and links represent interactions. Each node has two attributes, one is called 'fitness' and can assume any positive float value, the other is 'strategy' and contains an integer identifying the strategy this node pursues.

**Assumptions:**
* The interaction pattern is static, i.e. no links or nodes will appear or disappear over time.
* All interactions are equally weighted, meaning that an individual does not interact more often with one of its neighbours than with another.

### The Game
Let the two competing strategies be labelled $`0`$ and $`1`$. The game that the two strategies play is fully characterized by the so-called payoff matrix $`P \in \text{R}^{2 \times 2}`$, where $`p_{ij}, i, j \in \{0, 1\}`$ is the reward that an individual pursuing strategy $`i`$ gets from an interaction with an individual pursuing strategy $`j`$.

**Assumptions:**
* The payoff is constant, i.e. does not depend on environment parameters etc.
* The fitness of the agents depends only on the interactions and not, for example, on available resources etc.

### Update Rule
One iteration (corresponding to one time step) consists of the following three steps:
1. All players start with the same default fitness.
2. All nodes play the game against their nearest neighbours. Their fitness gets up- dated according to the strategies involved.
3. One node is chosen randomly and its strategy replaced by the strategy of its neighbour with highest fitness. If there are several nodes that assume the fitness maximum and if there is at least one node among them that has the same strategy as the chosen node, it does not change strategy.

One node changing its strategy can be interpreted in two ways. It can mean that the chosen individual dies and the fittest individual nearby reproduces, placing its offspring at the empty site. Or it can be pictured as the one individual learning or copying the strategy of its most successful neighbour.

**Assumptions:**
* Death (or change of strategy) is random and does not depend on, e.g., low fitness.
* Fitness is not accumulated or decreased over time, meaning the ’age’ of the nodes is irrelevant.
* Individuals have no knowledge about the strategy of other nodes, have no memory of past interactions and do not think rationally.

## Using The Simulation

### Technical Prerequisites

The code is written in Python, version 2.7. It requires the standard packages `matplotlib`, `numpy` and `random` and the non-standard package `networkx` for creating the graphs.

### The Simulator Class

The simulator class is the heart of the program. After initialization, an instance of it contains all the relevant information about the system and allows to perform the steps of the simulation. For creating a simulator, the following information is needed:
* `graph`: The graph representing the topology of interaction. Any graph produced with the package `networkx` may be used.
* `f_c`: The fraction of nodes with strategy $`0`$ in the initial state, i.e. a value between $`0`$ and $`1`$.
* `init`: A keyword indicating how the nodes with strategy $`0`$ are going to be distributed in the network.
Possible choices are:
    - `"init_random"`: Nodes will be selected at random.
    - `"init_hubs"`: Nodes will be selected by degree, highest degrees first.
    - `"init_spokes"`: Nodes will be selected by degree, lowest degrees first.
    - `"init_cluster"`: One node will be picked at random, then its neighbours, its neighbour's neighbours and so on.
* `payoff` : $`2 \times 2`$ matrix representing the payoff.
* `seed_id` (optional): For reproducible results, a seed for the random number generator may be given.

## Exemplary Runs

For the exemplary runs, payoff matrices representing the Prisoner's Dilemma are chosen, i.e. matrices of the form $`P = \begin{pmatrix}a & b\\ c & d\end{pmatrix}`$ with entries fulfilling the condition $` c > a > d > b`$.

Nodes with strategy $`0`$ are then called cooperators and nodes with strategy $`1`$ are defectors. For more background on the prisoner’s dilemma refer to [1]. The function scenarios from the module tools analysis provides two payoff matrices which both represent the prisoner’s dilemma, the first favouring the cooperating strategy, the second favouring the defecting strategy. These are also the scenarios that were used in the associated talk.

### Evolution On Random Regular Graphs

**File:** `01_evolution_on_random_regular_graphs.py`

**Main Goal:** Verify theoretical predictions about success of strategies.

The first exemplary run examines how the strategies cooperating and defecting evolve on a random regular graph (which is a graph where all nodes have the same degree k, i.e. the same number of nearest neighbours, but which nodes are connected is random). Tarnita et al. derived a condition based on the payoff matrix and the degree of nodes that allows to predict whether the cooperating or the defecting strategy will be more successful [2]. If

$`\sigma a + b > c + \sigma d \text{ with } \sigma = \frac{k+1}{k-1}`$

is true, then the cooperating strategy will be more successful, otherwise the defecting will be.

The script creates a random regular graph with a certain number of nodes with a certain degree (in the current setting the degree is 6). The simulation is run for both scenarios provided, and the number of cooperators is plotted over time (i.e. number of iterations). The validity of the prediction by the condition above for the given scenarios is then verified by determining which strategy the majority of nodes assumes. Of course, as there are infinite many possibilities to create a Prisoner's Dilemma, the condition cannot be verified for all of them, but so far all scenarios created showed agreement between the prediction and the actual evolution.

### Stable Minorities On Random Regular Graphs

**Files:** `02_minorities_on_random_regular_graphs.py`, `03_clustersizes_in_final_state.py`

**Main Goal:** Find possible configurations of stable minorities.

By having a look at the final state of simulations like the ones in the previous exemplary run, it is noticeable that the system always reaches a fixed state (meaning that no nodes will change their strategies any more). However, often there are several tiny clusters of one strategy among the majority. It is further apparent, that the sizes of these clusters differ for defector minorities in a cooperator majority and cooperator minorities in a defector majority.

The first script creates a histogram that shows the size distributions for both kinds of minority clusters. For this purpose, 100 simulations for both scenarios provided are performed and the sizes of all minorities that emerged are collected.

The results show that defector minorities consist of pairs almost exclusively, thus securing their continuity and maximising their fitness (as more defecting neighbours would decrease their payoff). The minimal size for cooperator minorities for the given setting seems to be 7, which is one more than the node degree.

The second script allows to search for a specific cluster size and plot the cluster and its nearest neighbours. The size of the nodes is proportional to its fitness. Searching for cooperator minorities with 7 nodes reveals that they always form a star-shaped cluster with one node in the middle which has only cooperator as neighbours. The cooperators, that are not in the middle have a very low fitness, but as they are connected to the center node, which has a high fitness, they will never change strategy and thus the cluster is stable.

### Success Of Cooperators On Scale-free Graphs

**File:** `04_success_of_strategies_on_scale_free_graphs.py`

**Main Goal:** Determine whether there are initial configurations that increase the success of a strategy in a scale-free graph.

Scale-free graphs are graphs where the degree distribution follows a power law. Due to this variability, the outcome of simulation on a scale-free graph depends on the initial distribution of strategies in the graph.

In order to determine which factors are beneficial for the spreading of a strategy, this script runs simulations for different initial configurations. These are determined by the fraction of cooperators and their distribution in the initial state. For each initial configuration, the simulation is run several hundred times. The output of the script is a table, showing for each initial configuration, the average number of simulations (out of 100) where the final state consisted of (1.) only cooperators, (2.) a majority of cooperators, (3.) only defectors, (4.) a majority of defectors.

The results show that if the hubs of the scale-free network are set to cooperating in the beginning, the cooperators will dominate the network most likely. That makes sense, as fitness increases also with the number of neighbours disregarding their strategy. Thus hubs have a high fitness advantage disregarding their strategy.

### Minorities On Scale-free Graphs

**File:** `05_minorities_on_scale_free_graphs.py`

**Main Goal** Compare the distribution of minority sizes between cooperators and defectors.

In order to see whether there are differences in the sizes of minority clusters (as seen for random regular graphs), we choose a setting where the advantage of one strategy over the other is not too great. In that way, final states with either cooperators or defector stable minorities arise more often. Then, a certain number of simulations are run and the cluster sizes of all stable minorities that occurred are collected. The result is represented as a histogram, showing the average number of occurrences of each cluster size per final state.

As expected, defectors tend to form smaller clusters whereas cooperators can also form big clusters, which is because both defectors and cooperators have higher fitness if they are connected to cooperators. Different to the cluster sizes emerging on random regular graphs is the fact that now all cluster sizes are possible, which is due to the variability of node degrees.