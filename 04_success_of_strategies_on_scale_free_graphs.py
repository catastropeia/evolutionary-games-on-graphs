"""
Analyze which initial configuration make the survival of one strategy 
more probable.
"""

from __future__ import division
from numpy import array, zeros, mean, std
from networkx import barabasi_albert_graph
from Simulator import Simulator


# --- Set parameters of the simulation. ---
n_nodes = 400
degree  = 3
payoff  = array([[ 3, 1],
                 [ 4, 2]])
               

# --- Set all initial configurations. ---
f_c_s = [0.5, 0.1, 0.01]
inits = ['init_random', 'init_cluster', 'init_hubs']


# --- Number of rounds (each with 100 simulations for each initial 
#     configuration). Time per round is ~100s. ---
n_rounds = 10


# --- As a data container for the results of the simulation, create a 
#     dictionary with one entry for each initial fraction of cooperators <f_c> 
#     and each initialization method <init>. Each entry is an array with  
#     dimension 4 x n_rounds to store for each round how often (out of 100 
#     simulations) the final state was cooperators only, a cooperator majority, 
#     defectors only or a defector majority. ---
res = dict()
for f_c in f_c_s:
    for init in inits:
        index = init + '_' + str(f_c)
        res[index] = zeros((4,n_rounds))


for j in range(n_rounds):
    print('Round ' + str(j))

    for i in range(100):
        graph = barabasi_albert_graph(n_nodes, degree)
        
        # --- Iterate over all initial configurations. ---
        for f_c in f_c_s:
            for init in inits:
    
                # --- Simulation. ---
                simulator = Simulator(init, f_c, graph, payoff)
                simulator.run()
                
                # --- Save results. ---
                index = init + '_' + str(f_c)
                if simulator.n_c[-1] == graph.number_of_nodes():
                    res[index][0,j] += 1
                elif simulator.n_c[-1] == 0:
                    res[index][2,j] += 1
                elif simulator.n_c[-1] >= 200:
                    res[index][1,j] += 1
                else:
                    res[index][3,j] += 1


# --- Print table showing how often cooperators and defectors won or were
#     majority for all given initial configurations. ---

print ' +-----------------------+---------------+---------------+---------------+'
print ' | \t \t \t |   f_c = {}   |   f_c = {}   |   f_c = {}  |'.format(f_c_s[0], f_c_s[1], f_c_s[2])
print ' +-----------------------+---------------+---------------+---------------+'

n_c_win = zeros((len(inits),len(f_c_s),2))
n_c_maj = zeros((len(inits),len(f_c_s),2))
n_d_win = zeros((len(inits),len(f_c_s),2))
n_d_maj = zeros((len(inits),len(f_c_s),2))

for j in range(len(inits)):    
    for i in range(len(f_c_s)):
        index  = inits[j] + '_' + str(f_c_s[i])
        n_c_win[j,i,0], n_c_win[j,i,1] = mean(res[index][0,:]), std(res[index][0,:])
        n_c_maj[j,i,0], n_c_maj[j,i,1] = mean(res[index][1,:]), std(res[index][1,:])
        n_d_win[j,i,0], n_d_win[j,i,1] = mean(res[index][2,:]), std(res[index][2,:])
        n_d_maj[j,i,0], n_d_maj[j,i,1] = mean(res[index][3,:]), std(res[index][3,:])

            
    print ' | ' + inits[j] + '\t n_c_win |  {:4.1f} +- {:4.1f} |  {:4.1f} +- {:4.1f} |  {:4.1f} +- {:4.1f} |' \
          .format(n_c_win[j,0,0],n_c_win[j,0,1],n_c_win[j,1,0],n_c_win[j,1,1],n_c_win[j,2,0],n_c_win[j,2,1])
    print ' | \t \t n_c_maj |  {:4.1f} +- {:4.1f} |  {:4.1f} +- {:4.1f} |  {:4.1f} +- {:4.1f} |' \
          .format(n_c_maj[j,0,0],n_c_maj[j,0,1],n_c_maj[j,1,0],n_c_maj[j,1,1],n_c_maj[j,2,0],n_c_maj[j,2,1])
    print ' | \t \t n_d_win |  {:4.1f} +- {:4.1f} |  {:4.1f} +- {:4.1f} |  {:4.1f} +- {:4.1f} |' \
          .format(n_d_win[j,0,0],n_d_win[j,0,1],n_d_win[j,1,0],n_d_win[j,1,1],n_d_win[j,2,0],n_d_win[j,2,1])
    print ' | \t \t n_d_maj |  {:4.1f} +- {:4.1f} |  {:4.1f} +- {:4.1f} |  {:4.1f} +- {:4.1f} |' \
          .format(n_d_maj[j,0,0],n_d_maj[j,0,1],n_d_maj[j,1,0],n_d_maj[j,1,1],n_d_maj[j,2,0],n_d_maj[j,2,1])
    print ' +-----------------------+---------------+---------------+---------------+'